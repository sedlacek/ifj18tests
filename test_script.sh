#!/bin/sh
# Ok lze najit na http://collabedit.com/ktpgj a sami zde muzete pridat pro ostatni testy
# Wrong lze najit na http://collabedit.com/42s8p a sami zde muzete pridat pro ostatni testu
# Pridavejte to co vite ze ma byt spatne nebo dobre
# Vyrobil Marek Sedlacek pokud najdete chybu nebo je neco chybne v testovacich kodech muzete mi napsat na discrodu napr @mark#7417

code_ok='test_code_ok.in'
code_ok_path='http://meetingwords.com/ep/pad/export/1Ow2YQW0lO/latest?format=txt'
code_wrong='test_code_wrong.in'
code_wrong_path='http://meetingwords.com/ep/pad/export/vOhiBCS7cH/latest?format=txt'
latest_test='latest_test.in'
sep='$'
error_amount=0


if [ $# -eq 1 ]; then
	if [ "$1" == "-h" ]; then
		echo "Pouziti: sh ./$0 path_to_compiler [ok_code_local wrong_code_local]"
		exit 0
	else
		wget -O $code_ok $code_ok_path
		if [ "$?" != "0" ]; then
			echo "INTERNI CHYBA: Nelze stahnout $code_ok_path"
			exit 1
		fi   
		wget -O $code_wrong $code_wrong_path 
		if [ "$?" != "0" ]; then
			echo "INTERNI CHYBA: Nelze stahnout $code_wrong_path"
			exit 1
		fi   
		echo ""
		echo "Online soubory stazene - Pokud jsou chybne je sance, ze jsi je stahl v dobe kdyz je nekdo zrovna upravuje"
	fi
elif [ $# -eq 3 ]; then
	echo ""
	echo "Pouziji se lokalni soubory '$2' a '$3'"
	code_ok=$2
	code_wrong=$3
else
	echo "Pouziti: sh ./$0 path_to_compiler [ok_code_local wrong_code_local]"
	exit 0
fi

COMPILER=$1

if [ ! -f "$COMPILER" ]; then
	echo ""
	echo "ERROR: Kopmpiler nebyl nalezen (cesta: $COMPILER)"
	exit 2
fi

echo "" > "$latest_test" # Prepsani souboru

test_code_ok(){	
	$COMPILER < "$latest_test" 1> /dev/null
	if [ "$?" != "0" ]; then
		echo "CHYBA: Kompiler interpretoval spravny kod jako chybny:"
		if [ "$DISPLAY_TEST" != 0 ]; then
			cat $latest_test
		fi
		if [ ! -z $STOP_AFTER_ERROR ]; then
			exit 1
		fi
		echo ""
		error_amount=$((error_amount+1))
	fi   
}

test_code_wrong(){
	#printf "Error: "
	grep -q '[^[:space:]]' < "$latest_test"
	if [ $? -ne 0 ]; then
		return 0
	fi
	$COMPILER < $latest_test 1> /dev/null 2>&1
	if [ "$?" == "0" ]; then
		echo "CHYBA: Kompiler interpretoval chybny kod jako spravny:"
		if [ "$DISPLAY_TEST" != 0 ]; then
			cat $latest_test
		fi
		if [ ! -z $STOP_AFTER_ERROR ]; then
			exit 1
		fi
		error_amount=$((error_amount+1))
	fi   
}

# Ok part

if [ ! -z $DISABLE_OK ]; then
	echo "WARNING: Ok_code se nekontroluje (je nastavene DISABLE_OK)"
else
	echo ""
	echo "Testovani korektnich vstupu. Kompiler by nemel zahlasit chybu"
	echo "----------------------------"
	echo "Testovaci kod se bere ze souboru '$code_ok'"
	echo ""
	while IFS='' read -r line || [[ -n "$line" ]]; do
		ret=$(echo $line | grep '^[[:space:]]*\$') 
		if [ $? -eq 0 ]; then
			test_code_ok
			echo "" > $latest_test
		else
			echo "$line" >> $latest_test
		fi
	done <"$code_ok"
fi

# Wrong code
if [ ! -z $DISABLE_WRONG ]; then
	echo "WARNING: Ok_code se nekontroluje (je nastavene DISABLE_OK)"
else
	echo ""
	echo "Testovani spatneho vstupu. Kompiler by musi zahlasit chybu"
	echo "----------------------------"
	echo "Testovaci kod se bere ze souboru '$code_wrong'"
	echo ""

	echo "" > $latest_test
	while IFS='' read -r line || [[ -n "$line" ]]; do
		ret=$(echo $line | grep '^[[:space:]]*\$') 
		if [ $? -eq 0 ]; then
			test_code_wrong
			echo "" > $latest_test
		else
			echo "$line" >> $latest_test
		fi
	done <"$code_wrong"
fi

echo

if [ $error_amount -eq 0 ]; then
	echo "VSECHNY TESTY PROBEHLY OK"
else
	echo "POCET NALEZENYCH CHYB: $error_amount"
fi
echo 

