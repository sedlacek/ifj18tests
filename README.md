# Testovací skript pro ifj18 kompiler

Skript prožene kompilerem postupně kod který je v pořádku, ale je šance, že kompiler tento sytax nezna a chybně nahlásí chybu.
Tento kód se stáhne z adresy (Ok kód)
	http://meetingwords.com/1Ow2YQW0lO
	
## ! Prosím také přijdete nějaký kód pro otestování, ale jenom ten co víte, že je správně !

Poté skript prožene kompilerem postupně kod který je je chybný, očekává se, že kompiler nahlásí chybu, nestane-li se tak, tento test to nahlásí.
Tento kód se stáhne z adresy (špatný kód)
	http://meetingwords.com/vOhiBCS7cH
	
## ! Prosím také přijdete nějaký kód pro otestování, ale jenom ten co víte, že je správně (tedy, že je chybný) !

Zatím je test jednoduchý a ani nekotroluje správnost návratového kódu apod. Pokud bude testů hodně, klidně tuto možnost ještě přidám.

# Formát online testovacích souborů

Kód který skript stáhne (z adres uvedených výše) má jednoduché formátování symbol `$` (dollar sign) je označen konec kódu (tedy začátek dalšího.
Tedy vše co je nad `$` se bere jako by to bylo ve vlastním souboru (a také se to tak poté nasimuluje)

## Příklad
```
a = 42
$
b = a
$
```
Je rovno tomu kdyby se do programu poslal soubor, jenž by obsahoval:
`a = 42`
A poté úplně jiný soubor kde by bylo:
`b = a`

Z toho je tedy jasné, že druhý kód (`b = a`) je špatný, za to první je správný.

# Použití Lokálních souborů
Lze to udělat a stačí pouze při volání skriptu jako druhý argument (po kompileru) poslat správně napsaný ifj18 kód (ve formátu s $) a jako třetí špatně napsaný

## Příklad
`sh ./test_script.sh ok_kod.in spatnej_kod.in`

# Vypnutí kontroly správného kódu
Stačí deklarovat v shellu proměnnou `DISABLE_OK`

## Příklad
`DISABLE_OK=1 sh ./test_script.sh`
Provede pouze kontrolu nad chybným kódem

# Vypnutí kontroly špatného kódu
Stačí deklarovat v shellu proměnnou `DISABLE_WRONG`

## Příklad
`DISABLE_WRONG=1 sh ./test_script.sh`
Provede pouze kontrolu nad správným kódem

# Vypnutí výpisu kódu na němž test selhal
Stačí nastavit proměnnou `DISPLAY_ERROR` na hodnotu 0

## Příklad
`DISABLE_OK=1 DISPLAY_ERROR=0 sh ./test_script.sh`

# Zastavení po nalezení první chyby
Stačí deklarovat proměnnou `STOP_AFTER_ERROR`

Jinak za správnost kódu co ostatní přidali neručím a doporučuji si tento kód zkontrolovat a pokud je nějaký chybný, tak ho smazat nebo zakomentovat ať to ostatním funguje. 
A stejně tak, jakmile se ifj odevzdá, tak tento repozitář nebudu nijak upravovat asi a kontrolní kódy také ne. 
